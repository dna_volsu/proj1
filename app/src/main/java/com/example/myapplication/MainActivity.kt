package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import com.example.myapplication.databinding.ActivityMainBinding

data class Entry (
    val name: String = "",
    val firstWord: String = "",
    val secondWord: String = "",
)

class MainActivity : AppCompatActivity() {

    private val entries: MutableMap<String, Entry> = mutableMapOf()

    private val TAG = "MainActivity"

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.fabWrite.setOnClickListener {
            entries[binding.nameInput.text.toString()] = Entry(
                name=binding.nameInput.text.toString(),
                firstWord=binding.firstWordInput.text.toString(),
                secondWord=binding.secondWordInput.text.toString(),
            )
            Log.w(TAG, entries.toString())
        }

        binding.fabRead.setOnClickListener {
            val name =  binding.nameInput.text.toString()
            binding.firstWordRead.text = entries[name]?.firstWord ?: getString(R.string.no_such_entry)
            binding.secondWordRead.text = entries[name]?.secondWord ?: getString(R.string.no_such_entry)
        }

        binding.fabSecond.setOnClickListener {
            startActivity(Intent(this, SecondActivity::class.java))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }
}